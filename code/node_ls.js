let path = process.argv[3]
let Params = process.argv[2]
if (!Params){Params = ''}
if (!process.argv[3]){
  path = process.argv[2]
}

let fs = require('fs')

fs.readdir(path, function(err, items) {
  const A = Params.indexOf('a') < 0 ? false : true
  const L = Params.indexOf('l') < 0 ? false : true
  const H = Params.indexOf('h') < 0 ? false : true

  if (items){
    for (let i=0; i<items.length; i++) {
      let file = path + items[i];

      if (A && !H && !L) {
        if (i<1){
          console.log('.')
          console.log('..')
        }
        ls(file, 'a')
      } else if (L && !H && !A) {
        AllList(path, file)
      } else if (H && !A && !L) {
        if (file.slice(3)[0] !== '.'){
          ls(file)
        }
      } else if (A && L && !H) {
        AllList(path, file)
      } else if (A && H && !L) {
        if (i<1){
          console.log('.')
          console.log('..')
        }
        ls(file, 'a')
      } else if (L && H && !A) {
        AllList(path, file, true)
      } else if (A && L && H) {
        AllList(path, file, true)
      } else {
        ls(file)
      }
    }
  }else{
      AllList(__dirname+'/'+path.slice(2), path)
  }

});

function ls(file){
    console.log(file)
}

function AllList(path,file, h=false){
  console.log(path, file, h)
  fs.stat(file, function(f) {
    return function(err, stats) {
      let D = (stats['mode'] & 0040000 ? 'd' : '-')
      let F = (stats['mode'] & 0100000 ? 'f' : '-')
      let DF = '';
      if (F === 'f'){
        DF = 'f'
      }else if(D === 'd'){
        DF = 'd'
      }else{
        DF = '-'
      }
      const mTime = new Date(fs.statSync(path).mtimeMs).toLocaleString('en-US', {
        month: 'short',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        hour12: false,
      });
      const Ruls = []
      Ruls.push(stats['mode'] & 400 ? 'r' : '-')
      Ruls.push(stats['mode'] & 200 ? 'w' : '-')
      Ruls.push(stats['mode'] & 100 ? 'x' : '-')
      Ruls.push(stats['mode'] & 40 ? 'r' : '-')
      Ruls.push(stats['mode'] & 20 ? 'w' : '-')
      Ruls.push(stats['mode'] & 10 ? 'x' : '-')
      Ruls.push(stats['mode'] & 4 ? 'r' : '-')
      Ruls.push(stats['mode'] & 2 ? 'w' : '-')
      Ruls.push(stats['mode'] & 1 ? 'x' : '-')
      Ruls.unshift(DF)

      function humanFileSize(size) {
          if (size === 0){return '0'}
          let i = Math.floor( Math.log(size) / Math.log(1024) );
          return ( size / Math.pow(1024, i) ).toFixed(2) * 1  + ['B', 'kB', 'MB', 'GB', 'TB'][i];
      };

      console.log(
          Ruls.join(''),
          stats.nlink,
          stats.uid,
          stats.gid,
          (h ? humanFileSize(stats['size']) : stats['size']),
          mTime,
          f.slice(2)
      )
    }
  }(file));
}


