const childProcess = require('child_process')
const readline = require('readline')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
})

let Params = {
  password: '',
  http_user: '',
  dir: '',
  branch: '',
  user_name: '',
  user_email: '',
  ssh_key_path: '',
  url: '',
  ssh_key: '',
}
for (let i = 0; i < process.argv.length; i++) {
  if (process.argv[i].slice(0, 12) === '--http-user=') {
    Params.http_user = process.argv[i].slice(12)
  } else if (process.argv[i].slice(0, 12) === '--http-pass=') {
    Params.password = process.argv[i].slice(12)
  } else if (process.argv[i].slice(0, 6) === '--dir=') {
    Params.dir = process.argv[i].slice(6)
  } else if (process.argv[i].slice(0, 9) === '--branch=') {
    Params.branch = process.argv[i].slice(9)
  } else if (process.argv[i].slice(0, 16) === '--git-user-name=') {
    Params.user_name = process.argv[i].slice(16)
  } else if (process.argv[i].slice(0, 17) === '--git-user-email=') {
    Params.user_email = process.argv[i].slice(17)
  } else if (process.argv[i].slice(0, 15) === '--ssh-key-path=') {
    Params.ssh_key_path = process.argv[i].slice(15)
  }
}
Params.url = process.argv.pop()

function execProcess(command) {
  return childProcess.execSync(command).toString()
}

if ((Params.url).slice(0, 5) === 'https') {
  rl.question('input directory to save : ', (answer) => {
    if (answer !== '') {
      Params.dir = answer
      branch(User_http)
    } else {
      console.log('Error, no dir')
      rl.close()
    }
  })

  function User_http() {
    if (Params.http_user === '') {
      rl.question('input user: ', (answer) => {
        if (answer) {
          Params.http_user = answer
          User_pass()
        } else {
          User_pass()
        }
      })
    } else {
      User_pass()
    }
  }

  function User_pass() {
    if (Params.password === '') {
      rl.question('input pass: ', (answer) => {
        if (answer) {
          Params.password = answer
        }
        GetDataFromGitHttp(Params.branch)
      })
    } else {
      GetDataFromGitHttp(Params.branch)
    }
  }

  function GetDataFromGitHttp(branch) {
    if (Params.password && Params.http_user) {
      childProcess.
          execSync(
              (`git clone https://${Params.http_user}:${Params.password}@${(Params.url).slice(
                  8)} ${branch ? `--branch ${branch}` : ''} ${Params.dir}`)).
          toString()
    } else {
      childProcess.
          execSync((`git clone ${branch ?
              `--branch ` + branch :
              ''} ${Params.url} ${Params.dir}`)).toString()
    }
    addGitConfig()
  }
} else {

  /*  SSH  */
  if (Params.dir === '') {
    rl.question('input directory to save :  ', (answer) => {
      if (answer) {
        Params.dir = answer
        /*  after dir, search key  */
        branch(Ssh_key_path)
      } else {
        console.log('Error, no dir')
        rl.close()
      }
    })
  } else {
    branch(Ssh_key_path)
  }
}

function branch(command) {
  if (Params.branch === '') {
    rl.question('input branch: ', (answer) => {
      if (answer) {
        Params.branch = answer
      }
      command()
    })
  } else {
    command()
  }
}

function searchKey(answ = 'false') {
  let answer = childProcess.execSync(
      `bash Search.sh ${Params.ssh_key_path || answ}`).toString()
  answer = answer.slice(0, answer.length - 1)
  if (answer === 'yes') {
    Params.ssh_key = childProcess.execSync(
        `cat ${Params.ssh_key_path || answ}`).toString()
    GetDataFromGitSSH()
  } else {
    childProcess.execSync(`ssh-keygen -t rsa -f ${Params.ssh_key_path || answ}`)
    Params.ssh_key = childProcess.execSync(
        `cat ${Params.ssh_key_path || answ}`).toString()
    /*  answer people  */
    console.log('copy this key from git :')
    console.log(childProcess.execSync(`cat ${Params.ssh_key_path || answ}.pub`).
        toString())
    SetKeyToGit()
  }
}

function Ssh_key_path() {
  if (Params.ssh_key_path) {
    searchKey()
  } else {
    rl.question('input path to key: ', (a) => {
      if (a) {
        Params.ssh_key_path = a
        searchKey(a)
      } else {
        console.log('Path no input')
        rl.close()
      }
    })
  }
}

function SetKeyToGit() {
  rl.question('add your key to git [yes/no]', (answer) => {
    if (answer === 'yes') {
      console.log('user add key from git')
      GetDataFromGitSSH()
    } else {
      console.log('Error no key from git')
      rl.close()
    }
  })
}

function GetDataFromGitSSH() {
  childProcess.execSync(
      `bash -c 'GIT_SSH_COMMAND="ssh -i ${Params.ssh_key_path} -F /dev/null" git clone  ${Params.branch ?
          `--branch ${Params.branch}` : ''} ${Params.url} ${Params.dir}'`)
  addGitConfig()
}

/*  GIT CONFIG USER.NAME && USER.EMAIL  */
function addGitConfig() {
  rl.question('input git config user.name: ', (name) => {
    rl.question('input git config user.email: ', (email) => {
      console.log(a)
      addGit(name, email)
    })
  })
}

function addGit(name, email) {
  childProcess.execSync(
      `cd ${Params.dir}; git config user.name ${name}`)

  childProcess.execSync(
      `cd ${Params.dir}; git config user.email ${email}`)
  rl.close()
}

///http @ poreshat



