const http = require('http')
const port = 3000
const cluster = require('cluster')

const requestHandler = (require, response) => {
    response.writeHead(200)
    response.end('=^..^=')
}

const server = http.createServer(requestHandler)

if (cluster.isMaster){
    const cpuCount = require('os').cpus.length
    console.log('i am yadro',cpuCount)
    for (let i = 0; i < cpuCount; i++){
        console.log('i am yadro', i)
        cluster.schedulingPolicy = cluster.SCHED_NONE
        cluster.fork()
    }

    /*  start  */
    cluster.on('fork', (worker) => {
        console.log('Worker # ',worker.id,' is online')
    })

    /*  work connecting in server  */
    cluster.on('listening', (worker, address) => {
        console.log('Worker # ',worker.id,' is now connected to ',JSON.stringify(address))
    })


    /*  worker disconnect  */
    cluster.on('disconnect', (worker) => {
        console.log('Worker # ',worker.id,' has disconnected')
    })

    /* end  */
    cluster.on('exit', (worker) => {
        console.log('Worker # ',worker.id,' is dead')
        /*  запускаем новый процес  */
        cluster.fork()
    })
}else{
    server.listen(port + cluster.worker.id, (err) => {
        if (err){
            return console.log('Server error ', error)
        }
        console.log('port ', port)
    })
}



