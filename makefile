build:
	docker build --no-cache ./docker/node -t servernode
up:
	docker run -d -p 3000:3000 --name container1 -v $(PWD)/code:/project servernode
shell:
	docker exec -it --user="1000" container1 bash
